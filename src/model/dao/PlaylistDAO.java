package model.dao;

import SpotNet.ConexaoPostGres;
import SpotNet.SpotNet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.entity.Playlist;

/**
 *
 * @author Rodrigo Cunha Rodrigues
 */
public class PlaylistDAO
{

    protected Connection connection;
    ConexaoPostGres conexao = new ConexaoPostGres();

    public List<Playlist> BuscaPlaylist(String nome)
    {
        connection = conexao.conectar();
        Statement stmt = null;
        List<Playlist> playlists = new ArrayList();

        String sql = "select * from playlist where nome like '%" + nome + "%'";
        System.out.println(sql);
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Playlist ObjUsu = new Playlist();

                try {
                    ObjUsu.setNome(rs.getString("nome"));
                    ObjUsu.setId_playlist(rs.getInt("id_playlist"));
                    ObjUsu.setUsuario(rs.getInt("usuario"));

                    playlists.add(ObjUsu);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            connection.close();

        } catch (SQLException ex) {
            Logger.getLogger(SpotNet.class.getName()).log(Level.SEVERE, null, ex);
        }

        return playlists;

    }

    public void AlteraPlaylist(String nome, int usuario, Playlist objPlaylist) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = "update playlist set nome='" + nome + "', usuario = "+ usuario +" where id_playlist=" + objPlaylist.getId_playlist() + "";
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }

    public void ExcluiPlaylist(Playlist objPlaylist) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = "delete from playlist where id_playlist=" + objPlaylist.getId_playlist() + "";
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }

    public void InserePlaylist(String nome, int usuario) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = "insert into playlist (nome, usuario)values(?, ?)";
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            preparedStmt.setString(1, nome);
            preparedStmt.setInt(2, usuario);

            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }
}

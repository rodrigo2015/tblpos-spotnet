package model.entity;

/**
 *
 * @author Rodrigo Cunha Rodrigues
 */
public class DiscoMusica
{
    private int id_disco_musica;
    private int id_disco;
    private int id_musica;

    public int getId_disco_musica()
    {
        return id_disco_musica;
    }

    public void setId_disco_musica(int id_disco_musica)
    {
        this.id_disco_musica = id_disco_musica;
    }

    public int getId_disco()
    {
        return id_disco;
    }

    public void setId_disco(int id_disco)
    {
        this.id_disco = id_disco;
    }

    public int getId_musica()
    {
        return id_musica;
    }

    public void setId_musica(int id_musica)
    {
        this.id_musica = id_musica;
    }
}

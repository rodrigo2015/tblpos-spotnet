/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import SpotNet.ConexaoPostGres;
import SpotNet.SpotNet;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.entity.Usuario;

/**
 *
 * @author Rodrigo Cunha Rodrigues
 */
public class UsuarioDAO
{

    protected Connection connection;
    ConexaoPostGres conexao = new ConexaoPostGres();

    public List<Usuario> BuscaUsuario(String nome)
    {
        connection = conexao.conectar();
        Statement stmt = null;
        List<Usuario> usuarios = new ArrayList();

        String sql = "select * from usuario where nome like '%" + nome + "%'";
        System.out.println(sql);
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Usuario ObjUsu = new Usuario();

                try {
                    ObjUsu.setNome(rs.getString("nome"));
                    ObjUsu.setId_usuario(rs.getInt("id_usuario"));

                    usuarios.add(ObjUsu);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            connection.close();

        } catch (SQLException ex) {
            Logger.getLogger(SpotNet.class.getName()).log(Level.SEVERE, null, ex);
        }

        return usuarios;

    }
    
    public Usuario BuscaUsuarioPorId(int idUsuario)
    {
        connection = conexao.conectar();
        Statement stmt = null;

        String sql = "select * from usuario where id_usuario = " + idUsuario;
        System.out.println(sql);
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Usuario ObjUsu = new Usuario();

                try {
                    ObjUsu.setNome(rs.getString("nome"));
                    ObjUsu.setId_usuario(rs.getInt("id_usuario"));

                    return ObjUsu;

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            connection.close();

        } catch (SQLException ex) {
            Logger.getLogger(SpotNet.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    public void AlteraUsuario(String nome, Usuario objUsuario) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = "update usuario set nome='" + nome + "' where id_usuario=" + objUsuario.getId_usuario() + "";
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }

    public void ExcluiUsuario(Usuario objUsuario) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = "delete from usuario where id_usuario=" + objUsuario.getId_usuario() + "";
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }

    public void InsereUsuario(String nome) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = " insert into usuario (nome)values(?)";
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            preparedStmt.setString(1, nome);

            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }

}

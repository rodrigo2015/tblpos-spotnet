/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import SpotNet.ConexaoPostGres;
import SpotNet.SpotNet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.entity.Disco;

/**
 *
 * @author Rodrigo Cunha Rodrigues
 */
public class DiscoDAO
{

    protected Connection connection;
    ConexaoPostGres conexao = new ConexaoPostGres();

    public List<Disco> BuscaDisco(String nome)
    {
        connection = conexao.conectar();
        Statement stmt = null;
        List<Disco> discos = new ArrayList();

        String sql = "select * from disco where nome like '%" + nome + "%'";
        System.out.println(sql);
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Disco ObjUsu = new Disco();

                try {
                    ObjUsu.setNome(rs.getString("nome"));
                    ObjUsu.setId_disco(rs.getInt("id_disco"));

                    discos.add(ObjUsu);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            connection.close();

        } catch (SQLException ex) {
            Logger.getLogger(SpotNet.class.getName()).log(Level.SEVERE, null, ex);
        }

        return discos;

    }

    public void AlteraDisco(String nome, Disco objDisco) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = "update disco set nome='" + nome + "' where id_disco=" + objDisco.getId_disco() + "";
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }

    public void ExcluiDisco(Disco objDisco) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = "delete from disco where id_disco=" + objDisco.getId_disco() + "";
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }

    public void InsereDisco(String nome) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = " insert into disco (nome)values(?)";
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            preparedStmt.setString(1, nome);

            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }

}

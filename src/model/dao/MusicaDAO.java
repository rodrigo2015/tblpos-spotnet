/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import SpotNet.ConexaoPostGres;
import SpotNet.SpotNet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.entity.Musica;

/**
 *
 * @author Rodrigo Cunha Rodrigues
 */
public class MusicaDAO
{

    protected Connection connection;
    ConexaoPostGres conexao = new ConexaoPostGres();

    public List<Musica> BuscaMusica(String nome)
    {
        connection = conexao.conectar();
        Statement stmt = null;
        List<Musica> musicas = new ArrayList();

        String sql = "select * from musica where nome like '%" + nome + "%'";
        System.out.println(sql);
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Musica ObjUsu = new Musica();

                try {
                    ObjUsu.setNome(rs.getString("nome"));
                    ObjUsu.setId_musica(rs.getInt("id_musica"));
                    ObjUsu.setAutor(rs.getInt("autor"));

                    musicas.add(ObjUsu);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            connection.close();

        } catch (SQLException ex) {
            Logger.getLogger(SpotNet.class.getName()).log(Level.SEVERE, null, ex);
        }

        return musicas;

    }
    
    public Musica BuscaMusicaPorId(int idMusica)
    {
        connection = conexao.conectar();
        Statement stmt = null;

        String sql = "select * from musica where id_musica = " + idMusica;
        System.out.println(sql);
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Musica ObjMusica = new Musica();

                try {
                    ObjMusica.setNome(rs.getString("nome"));
                    ObjMusica.setId_musica(rs.getInt("id_musica"));
                    ObjMusica.setAutor(rs.getInt("autor"));

                    return ObjMusica;

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            connection.close();

        } catch (SQLException ex) {
            Logger.getLogger(SpotNet.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    public void AlteraMusica(String nome, int autor, Musica objMusica) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = "update musica set nome='" + nome + "', autor = "+ autor +" where id_musica=" + objMusica.getId_musica() + "";
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }

    public void ExcluiMusica(Musica objMusica) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = "delete from musica where id_musica=" + objMusica.getId_musica() + "";
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }

    public void InsereMusica(String nome, int autor) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = "insert into musica (nome, autor)values(?, ?)";
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            preparedStmt.setString(1, nome);
            preparedStmt.setInt(2, autor);

            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import SpotNet.ConexaoPostGres;
import SpotNet.SpotNet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.entity.DiscoMusica;

/**
 *
 * @author Rodrigo Cunha Rodrigues
 */
public class DiscoMusicaDAO
{

    protected Connection connection;
    ConexaoPostGres conexao = new ConexaoPostGres();

    public List<DiscoMusica> BuscaMusicasPorIdDisco(int idDisco)
    {
        connection = conexao.conectar();
        Statement stmt = null;
        List<DiscoMusica> discosMusicas = new ArrayList();

        String sql = "select * from disco_musica where disco = " + idDisco;
        System.out.println(sql);
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                DiscoMusica ObjDiscoMusica = new DiscoMusica();

                try {
                    ObjDiscoMusica.setId_musica(rs.getInt("musica"));
                    ObjDiscoMusica.setId_disco(rs.getInt("disco"));
                    discosMusicas.add(ObjDiscoMusica);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            connection.close();

        } catch (SQLException ex) {
            Logger.getLogger(SpotNet.class.getName()).log(Level.SEVERE, null, ex);
        }

        return discosMusicas;

    }

    public void ExcluiDiscoMusica(int idDisco, int idMusica) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = "delete from disco_musica where disco = " + idDisco + 
                            " AND musica = " + idMusica;
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }
    
    public void ExcluiPorIdDisco(int idDisco) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = "delete from disco_musica where disco = " + idDisco;
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }

    public void InsereDiscoMusica(int idDisco, int idMusica) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = "insert into disco_musica (disco, musica)values(?, ?)";
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            preparedStmt.setInt(1, idDisco);
            preparedStmt.setInt(2, idMusica);

            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }

}

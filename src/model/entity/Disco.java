package model.entity;

/**
 *
 * @author Rodrigo Cunha Rodrigues
 */
public class Disco
{

    int id_disco;
    String nome;

    public int getId_disco()
    {
        return id_disco;
    }

    public void setId_disco(int id_disco)
    {
        this.id_disco = id_disco;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }
}

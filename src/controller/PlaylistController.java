/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.dao.PlaylistDAO;
import model.entity.Playlist;

/**
 *
 * @author Rodrigo Cunha Rodrigues
 */
public class PlaylistController
{

    PlaylistDAO objDAO = new PlaylistDAO();
    List<Playlist> usuarios = new ArrayList();

    public List<Playlist> BuscaPlaylist(String nome)
    {
        return objDAO.BuscaPlaylist(nome);
    }

    public void InserePlaylist(String nome, int usuario) throws SQLException
    {
        objDAO.InserePlaylist(nome, usuario);
    }

    public void AlteraPlaylist(String nome, int usuario, Playlist objPlaylist) throws SQLException
    {
        objDAO.AlteraPlaylist(nome, usuario, objPlaylist);
    }

    public void ExcluiPlaylist(Playlist objPlaylist) throws SQLException
    {
        objDAO.ExcluiPlaylist(objPlaylist);
    }
}

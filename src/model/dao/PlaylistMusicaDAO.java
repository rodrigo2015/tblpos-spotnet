package model.dao;

import SpotNet.ConexaoPostGres;
import SpotNet.SpotNet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.entity.PlaylistMusica;

/**
 *
 * @author Rodrigo Cunha Rodrigues
 */
public class PlaylistMusicaDAO
{

    protected Connection connection;
    ConexaoPostGres conexao = new ConexaoPostGres();

    public List<PlaylistMusica> BuscaMusicasPorIdPlaylist(int idPlaylist)
    {
        connection = conexao.conectar();
        Statement stmt = null;
        List<PlaylistMusica> playlistMusicas = new ArrayList();

        String sql = "select * from playlist_musica where playlist = " + idPlaylist;
        System.out.println(sql);
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PlaylistMusica ObjPlaylistMusica = new PlaylistMusica();

                try {
                    ObjPlaylistMusica.setMusica(rs.getInt("musica"));
                    ObjPlaylistMusica.setPlaylist(rs.getInt("playlist"));
                    playlistMusicas.add(ObjPlaylistMusica);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            connection.close();

        } catch (SQLException ex) {
            Logger.getLogger(SpotNet.class.getName()).log(Level.SEVERE, null, ex);
        }

        return playlistMusicas;

    }

    public void ExcluiPlaylistMusica(int idPlaylist, int idMusica) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = "delete from playlist_musica where playlist = " + idPlaylist + 
                            " AND musica = " + idMusica;
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }
    
    public void ExcluiPorIdPlaylist(int idPlaylist) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = "delete from playlist_musica where playlist = " + idPlaylist;
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }

    public void InserePlaylistMusica(int idPlaylist, int idMusica) throws SQLException
    {
        connection = conexao.conectar();
        Statement stmt = null;

        try {
            String query = "insert into playlist_musica (playlist, musica)values(?, ?)";
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            preparedStmt.setInt(1, idPlaylist);
            preparedStmt.setInt(2, idMusica);

            System.out.println(preparedStmt);
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.commit();
            connection.close();
        }
    }

}

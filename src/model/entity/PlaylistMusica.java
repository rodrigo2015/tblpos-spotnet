package model.entity;

/**
 *
 * @author Rodrigo Cunha Rodrigues
 */
public class PlaylistMusica
{
    private int id_playlist_musica;
    private int playlist;
    private int musica;

    public int getId_playlist_musica()
    {
        return id_playlist_musica;
    }

    public void setId_playlist_musica(int id_playlist_musica)
    {
        this.id_playlist_musica = id_playlist_musica;
    }

    public int getPlaylist()
    {
        return playlist;
    }

    public void setPlaylist(int playlist)
    {
        this.playlist = playlist;
    }

    public int getMusica()
    {
        return musica;
    }

    public void setMusica(int musica)
    {
        this.musica = musica;
    }

    
}

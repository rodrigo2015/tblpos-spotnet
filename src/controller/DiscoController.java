/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTextField;
import model.dao.DiscoDAO;
import model.entity.Disco;

/**
 *
 * @author Rodrigo Cunha Rodrigues
 */
public class DiscoController
{

    DiscoDAO objDAO = new DiscoDAO();
    List<Disco> autores = new ArrayList();

    public List<Disco> BuscaDisco(String nome)
    {
        return objDAO.BuscaDisco(nome);
    }

    public void InsereDisco(String nome) throws SQLException
    {
        objDAO.InsereDisco(nome);
    }

    public void AlteraDisco(String nome, Disco objDisco) throws SQLException
    {
        objDAO.AlteraDisco(nome, objDisco);
    }

    public void ExcluiDisco(Disco objDisco) throws SQLException
    {
        objDAO.ExcluiDisco(objDisco);
    }
}

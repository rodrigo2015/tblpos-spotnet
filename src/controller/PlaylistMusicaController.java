package controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.dao.PlaylistMusicaDAO;
import model.entity.PlaylistMusica;

/**
 *
 * @author Rodrigo Cunha Rodrigues
 */
public class PlaylistMusicaController
{

    PlaylistMusicaDAO objDAO = new PlaylistMusicaDAO();
    List<PlaylistMusica> autores = new ArrayList();

    public List<PlaylistMusica> BuscaMusicasPorIdPlaylist(int idPlaylist)
    {
        return objDAO.BuscaMusicasPorIdPlaylist(idPlaylist);
    }

    public void InserePlaylistMusica(int idPlaylist, int idMusica) throws SQLException
    {
        objDAO.InserePlaylistMusica(idPlaylist, idMusica);
    }

    public void ExcluiPlaylistMusica(int idPlaylist, int idMusica) throws SQLException
    {
        objDAO.ExcluiPlaylistMusica(idPlaylist, idMusica);
    }
    public void ExcluiPorIdPlaylist(int idPlaylist) throws SQLException
    {
        objDAO.ExcluiPorIdPlaylist(idPlaylist);
    }
}

package model.entity;

/**
 *
 * @author Rodrigo Cunha
 */
public class Playlist
{

    int id_playlist;
    int usuario;
    String nome;

    public int getId_playlist()
    {
        return id_playlist;
    }

    public void setId_playlist(int id_playlist)
    {
        this.id_playlist = id_playlist;
    }

    public int getUsuario()
    {
        return usuario;
    }

    public void setUsuario(int usuario)
    {
        this.usuario = usuario;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

}

package model.entity;

/**
 *
 * @author Rodrigo Cunha Rodrigues
 */
public class Usuario
{

    int id_usuario;
    String nome;

    public int getId_usuario()
    {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario)
    {
        this.id_usuario = id_usuario;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }
}

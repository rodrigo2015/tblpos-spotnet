/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.dao.MusicaDAO;
import model.entity.Musica;

/**
 *
 * @author Rodrigo Cunha Rodrigues
 */
public class MusicaController
{

    MusicaDAO objDAO = new MusicaDAO();
    List<Musica> autores = new ArrayList();

    public List<Musica> BuscaMusica(String nome)
    {
        return objDAO.BuscaMusica(nome);
    }
    
    public Musica BuscaMusicaPorId(int idMusica)
    {
        return objDAO.BuscaMusicaPorId(idMusica);
    }

    public void InsereMusica(String nome, int autor) throws SQLException
    {
        objDAO.InsereMusica(nome, autor);
    }

    public void AlteraMusica(String nome, int autor, Musica objMusica) throws SQLException
    {
        objDAO.AlteraMusica(nome, autor, objMusica);
    }

    public void ExcluiMusica(Musica objMusica) throws SQLException
    {
        objDAO.ExcluiMusica(objMusica);
    }
}

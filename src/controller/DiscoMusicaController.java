/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.dao.DiscoMusicaDAO;
import model.entity.DiscoMusica;

/**
 *
 * @author Rodrigo Cunha Rodrigues
 */
public class DiscoMusicaController
{

    DiscoMusicaDAO objDAO = new DiscoMusicaDAO();
    List<DiscoMusica> autores = new ArrayList();

    public List<DiscoMusica> BuscaMusicasPorIdDisco(int idDisco)
    {
        return objDAO.BuscaMusicasPorIdDisco(idDisco);
    }

    public void InsereDiscoMusica(int idDisco, int idMusica) throws SQLException
    {
        objDAO.InsereDiscoMusica(idDisco, idMusica);
    }

    public void ExcluiDiscoMusica(int idDisco, int idMusica) throws SQLException
    {
        objDAO.ExcluiDiscoMusica(idDisco, idMusica);
    }
    public void ExcluiPorIdDisco(int idDisco) throws SQLException
    {
        objDAO.ExcluiPorIdDisco(idDisco);
    }
}

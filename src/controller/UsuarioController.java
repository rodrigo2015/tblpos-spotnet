/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.dao.UsuarioDAO;
import model.entity.Usuario;

/**
 *
 * @author Rodrigo Cunha Rodrigues
 */
public class UsuarioController
{

    UsuarioDAO objDAO = new UsuarioDAO();
    List<Usuario> autores = new ArrayList();

    public List<Usuario> BuscaUsuario(String nome)
    {
        return objDAO.BuscaUsuario(nome);
    }
    
    public Usuario BuscaUsuarioPorId(int idUsuario)
    {
        return objDAO.BuscaUsuarioPorId(idUsuario);
    }

    public void InsereUsuario(String nome) throws SQLException
    {
        objDAO.InsereUsuario(nome);
    }

    public void AlteraUsuario(String nome, Usuario objUsuario) throws SQLException
    {
        objDAO.AlteraUsuario(nome, objUsuario);
    }

    public void ExcluiUsuario(Usuario objUsuario) throws SQLException
    {
        objDAO.ExcluiUsuario(objUsuario);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entity;

import javax.sound.sampled.AudioFileFormat;

/**
 *
 * @author contdiego
 */
public class Musica
{

    int id_musica;
    int autor;
    String nome;

    public int getId_musica()
    {
        return id_musica;
    }

    public void setId_musica(int id_musica)
    {
        this.id_musica = id_musica;
    }

    public int getAutor()
    {
        return autor;
    }

    public void setAutor(int autor)
    {
        this.autor = autor;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

}
